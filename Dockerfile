# Source images
FROM "docker:dind"

# Ignore the warnings about being root
ENV PIP_ROOT_USER_ACTION="ignore"

# Ignore the version restraints, we are running latest stable anyway
# hadolint ignore=DL3018
RUN apk "add" --no-cache \
	"python3" \
	"python3-dev" \
	"curl" \
	"build-base" 

# Install PIP via Python to avoid problems with package manager
# hadolint ignore=DL3013,DL3059
RUN python3 -m "ensurepip" --upgrade
# hadolint ignore=DL3013,DL3059
RUN python3 -m "pip" "install" --upgrade --no-cache-dir "pip"

# Get the missing wheel
# hadolint ignore=DL3013,DL3059
RUN python3 -m "pip" "install" --no-cache-dir \
	"wheel"

# Try to avoid errors with ruamel library
# hadolint ignore=DL3013,DL3059
RUN python3 -m "pip" "install" --no-cache-dir \
	"ruamel.yaml"

# Install the actual packages
# hadolint ignore=DL3013,DL3059
RUN python3 -m "pip" "install" --no-cache-dir \
	"ansible" \
	"ansible-lint" \
	"molecule" \
	"molecule-docker" \
	"yamllint"

ENTRYPOINT [ "/bin/sh" ]
