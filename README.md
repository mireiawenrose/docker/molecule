![Build Status](https://img.shields.io/gitlab/pipeline-status/mireiawenrose/docker/molecule?branch=master&style=plastic)

# Molecule container
A simple build of [Molecule](https://molecule.readthedocs.io/en/latest/index.html) to run Ansible tests, based on Molecule [CI/CD documentation](https://molecule.readthedocs.io/en/latest/ci.html#gitlab-ci).
